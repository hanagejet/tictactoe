package com.hanagejet.tictactoe;

import android.animation.ObjectAnimator;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.larvalabs.svgandroid.SVG;
import com.larvalabs.svgandroid.SVGParser;

import java.util.Set;


public class TicTacToe extends Activity {

    private final int WC = ViewGroup.LayoutParams.WRAP_CONTENT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tic_tac_toe);

        OutPutGame output = new OutPutGame();
        SetUpGame setup = new SetUpGame();

        // 各ボタンのクリックイベント
        setup.button1.setOnClickListener(output.buttonClickListener);
        setup.button2.setOnClickListener(output.buttonClickListener);
        setup.button3.setOnClickListener(output.buttonClickListener);
        setup.button4.setOnClickListener(output.buttonClickListener);
        setup.button5.setOnClickListener(output.buttonClickListener);
        setup.button6.setOnClickListener(output.buttonClickListener);
        setup.button7.setOnClickListener(output.buttonClickListener);
        setup.button8.setOnClickListener(output.buttonClickListener);
        setup.button9.setOnClickListener(output.buttonClickListener);

        // リセットボタンのイベント
        setup.reset.setOnClickListener(output.resetClickListener);
    }

//    public void animationAlpha(ImageView target) {
//        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(target, "alpha", 0f, 1f);
//        objectAnimator.setDuration(300);
//        objectAnimator.start();
//    }

//    public void setUpAnimation() {
//        Resources res = getResources();
//        Bitmap bitmap = BitmapFactory.decodeResource(res, R.drawable.riki);
//        ImageView image = new ImageView(this);
//        image.setImageBitmap(bitmap);
        // image.setVisibility(View.INVISIBLE);
//
//        setContentView(image, new ActionBar.LayoutParams(100, 100));
//        animationAlpha(image);
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.tic_tac_toe, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    class OutPutGame extends SetUpGame {

        OnClickListener buttonClickListener = new OnClickListener() {
            @Override
            public void onClick(View view) {
                int id = 0;

                switch (view.getId()) {
                    case R.id.button1:
                        setUpTicTacToe(button1, id);
                        break;

                    case R.id.button2:
                        id = 1;
                        setUpTicTacToe(button2, id);
                        break;

                    case R.id.button3:
                        id = 2;
                        setUpTicTacToe(button3, id);
                        break;

                    case R.id.button4:
                        id = 3;
                        setUpTicTacToe(button4, id);
                        break;

                    case R.id.button5:
                        id = 4;
                        setUpTicTacToe(button5, id);
                        break;

                    case R.id.button6:
                        id = 5;
                        setUpTicTacToe(button6, id);
                        break;

                    case R.id.button7:
                        id = 6;
                        setUpTicTacToe(button7, id);
                        break;

                    case R.id.button8:
                        id = 7;
                        setUpTicTacToe(button8, id);
                        break;

                    case R.id.button9:
                        id = 8;
                        setUpTicTacToe(button9, id);
                        break;
                }
            }
        };

        // Activityリロード
        OnClickListener resetClickListener = new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = getIntent();
                overridePendingTransition(0, 0);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                finish();

                overridePendingTransition(0, 0);
                startActivity(intent);
            }
        };
    }

    class SetUpGame {
        // 各ボタン変数作成
        Button button1 = (Button) findViewById(R.id.button1);
        Button button2 = (Button) findViewById(R.id.button2);
        Button button3 = (Button) findViewById(R.id.button3);
        Button button4 = (Button) findViewById(R.id.button4);
        Button button5 = (Button) findViewById(R.id.button5);
        Button button6 = (Button) findViewById(R.id.button6);
        Button button7 = (Button) findViewById(R.id.button7);
        Button button8 = (Button) findViewById(R.id.button8);
        Button button9 = (Button) findViewById(R.id.button9);

        // リセットボタン
        Button reset = (Button) findViewById(R.id.reset);

        public int count = 0;

        public int[] table = new int[] {
                0, 0, 0,
                0, 0, 0,
                0, 0, 0
        };

        public void judgeGame() {
            // 勝敗判定配列
            int[] judge = new int[8];

            // 横
            judge[0] = table[0] + table[1] + table[2];
            judge[1] = table[3] + table[4] + table[5];
            judge[2] = table[6] + table[7] + table[8];

            // 縦
            judge[3] = table[0] + table[3] + table[6];
            judge[4] = table[1] + table[4] + table[7];
            judge[5] = table[2] + table[5] + table[8];

            // 斜
            judge[6] = table[0] + table[4] + table[8];
            judge[7] = table[2] + table[4] + table[6];

            TextView judgeText = (TextView) findViewById(R.id.judge_text);
            // 勝敗判定
            for(int i = 0; i <= judge.length -1; i++) {
                if(judge[i] == 3) {
                    judgeText.setText("◯ is win!");
                    resetGame();
                    break;
                } else if (judge[i] == -3) {
                    judgeText.setText("☓ is win!");
                    resetGame();
                    break;
                } else if ((count == 9 && judge[i] != 3) || (count == 9 && judge[i] != -3)) {
                    judgeText.setText("A Drawn Tie!");
                    resetGame();
                }
            }
        }

        public void resetGame() {
            // リセットボタン表示
            reset.setVisibility(View.VISIBLE);

            // ボタンクリック無効
            button1.setClickable(false);
            button2.setClickable(false);
            button3.setClickable(false);
            button4.setClickable(false);
            button5.setClickable(false);
            button6.setClickable(false);
            button7.setClickable(false);
            button8.setClickable(false);
            button9.setClickable(false);
        }

        // ボタンクリック時のメソッド
        public void setUpTicTacToe(Button button, int x) {
            // ◯☓設置
            if(table[x] == 0) {
                if(count % 2 == 0) {
                    button.setText("O");
                    button.setBackgroundColor(Color.parseColor("#5677fc"));
                    button.setTextColor(Color.parseColor("#ffffff"));
                    table[x] += 1;
                } else {
                    button.setText("X");
                    button.setBackgroundColor(Color.parseColor("#e51c23"));
                    button.setTextColor(Color.parseColor("#ffffff"));
                    table[x] += -1;
                }
                count++;
            }

            judgeGame();
        }
    }
}